<div class='custom'>
	<div class='row'>
		<div class='well'>
			<h2>Contact Persons</h2>
			<br />
			<table class='table table-bordered table-striped'>
		    <thead>
			    <tr>
			    	<th>Role</th>
				    <th>Contact Person </th>
				    <th>Phone Number </th>
			    </tr>
		    </thead>
		    <tbody>
		    	<tr>
					<td>Head</td>
					<td>Naa Tso Otu-Boateng</td>
					<td>0242379350</td>
				</tr>
				<tr>
					<td>Asst. Head</td>
					<td>Rosie Ampah</td>
					<td>0244063748</td>
				</tr>
			   <tr>
			   		<td>Registration</td>
					<td>Andrew Amegatcher</td>
					<td>0267666975</td>
				</tr>
				<tr>
					<td>Registration</td>
					<td>Anthony Dzixose-Davor</td>
					<td>0244226692</td>
				</tr>
				
				<tr>
					<td>Payments</td>
					<td>Esther Kwofie</td>
					<td>0260774349</td>
				</tr>
				<tr>
					<td>Payments</td>
					<td>Robin Huw Barnes</td>
					<td>0543752472 </td>
				</tr>
				<tr>
					<td>Payments</td>
					<td>Thomas Olubumi</td>
					<td>0265738884</td>
				</tr>
				<tr>
					<td>Finance / Donations</td>
					<td>Kukua Pratt</td>
					<td>0244574675</td>
				</tr>
		    </tbody>
	    </table>
		</div>
	    
	</div>
</div>
