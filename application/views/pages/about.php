<div class='custom'>
	<div class="row ">
		<div class='well'>
			<h2>About the Teen Camp</h2>
			<br />
			<div>
				<p>
					The Accra Ridge Church Teen Camp is an annual 5 day retreat to allow teens a time of 
					retreat from daily routines and pressures; to reflect on what their relationships with 
					God really mean, and to discover new depths in their understanding of and approach to
					Christianity. 
				</p>
				
				<p>
					The Teen Camp programme comprises structured and unstructured sessions, talks, bible studies,
					games and recreational activities, fasting, prayer sessions, and counselling designed to
					achieve this objective.
				</p>
				<p>
					Teen Camp is in its eleventh year and has been a blessing year after year, now not to just the 
					teenagers and youth of Accra Ridge Church but those of other churches too. In the past three 
					years, it has attracted so many campers and to the glory of God the numbers have risen year 
					after year.
				</p>
				<br>
				<p>
					<h4>Teen Camp 2012</h4>
					The theme for this year’s camp is “Soldiers for Christ” – Holy, Steady, Battle-Ready! The theme 
					verse is from 2 Timothy 2:3-4. 
					The purpose of this year’s camp is to impress essential facets of the Christian faith to teenagers, 
					through allegorical reference to elements of military life: recruitment, mission, battle gear, advancement, 
					communication lines and war injuries. 
					We hope to motivate campers to view their salvation as enrolment in God’s army; and embark on their faith as a 
					lifelong mission. 
				</p>

				<p>
					If you are between the ages of 11 and 18, enroll at this year’s teen camp and join the Soldiers for Christ.
				</p>
			</div>
		</div>		
	</div>
</div>